class Tweet < ApplicationRecord
  belongs_to :user

  validates :body, length: { maximum: 180 }

  validate :tweet_uniqueness, on: :create

  private

  def tweet_uniqueness
    tweets = Tweet.where(body: body, created_at: [1.day.ago..DateTime.now] )
    return unless tweets.present?

    errors.add(:body, 'Duplicated Tweet')
  end

end
