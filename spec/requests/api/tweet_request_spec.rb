require 'rails_helper'

RSpec.describe "API Tweets", type: :request do
  describe "#create" do
    let(:response_body) { JSON.parse(response.body) }

    context 'with valid parameters' do
      let(:user1) { create(:user)}
      let(:valid_body) { 'This is a valid tweet' }

      it 'returns a successful response' do
        post api_tweets_path(user_id: user1.id, body: valid_body)

        expect(response).to have_http_status(:success)
      end

      it 'creates a new tweet' do
        expect {
          post api_tweets_path(user_id: user1.id, body: valid_body)
        }.to change(Tweet, :count).by(1)
      end
    end

    context 'With invalid parameters' do
      let(:user1) { create(:user)}

      subject { post api_tweets_path(user_id: user1.id, body: body) }

      context 'When the body is too long' do
        let(:body) { 'a' * 181 }

        it 'returns an error response' do
          subject
          expect(response).to have_http_status(400)
        end

        it 'does not create a new tweet' do
          expect { subject }.to change(Tweet, :count).by(0)
        end
      end

      context 'When the tweet might be a duplicate' do

        let(:body) { 'Duplicated Tweet' }

        before do
          Tweet.create(user_id: user1.id, body: body)
        end

        it 'returns an error response' do
          subject
          expect(response).to have_http_status(400)
        end

        it 'does not create a new tweet' do
          expect { subject }.to change(Tweet, :count).by(0)
        end
      end
    end
  end

  describe "#index" do
    let(:tweet) { create(:tweet) }

    before do
      tweet
    end

    subject { get api_tweets_path(page: 0) }

    context 'success' do
      it 'return tweets' do
        subject

        debugger
      end
    end
  end
end
